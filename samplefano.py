# Copyright (C) 2021, Luca Baldini.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import numpy as np
import scipy.stats
from scipy.special import factorial

from matplotlib_ import plt


CRITICAL_FANO = 3. - 2. * np.sqrt(2.)



def discrete_prob_dist_stat(k, P):
    """Utility function to calculate the basic statistics for a generic discrete
    probability distribution.

    Arguments
    ---------
    k : array_like
        The (discrete) values that the random variable can take.

    P : array_like
        The probability values corresponding to the k values.

    Return
    ------
    The normalization, mean and variance of the distribution.
    """
    norm = P.sum()
    mean = (k * P).sum()
    variance = ((k - mean)**2. * P).sum()
    return norm, mean, variance


def discrete_chisquare_test(k, P, rv):
    """Simple chisquare test for a discrete distribution.
    """
    _, obs = np.unique(rv, return_counts=True)
    exp = P * len(rv)
    chisq, p_value = scipy.stats.chisquare(obs, exp)
    if p_value > 0.5:
        p_value = 1. - p_value
    ndof = len(k) - 1
    return chisq, ndof, p_value


def _discrete_random_variate(k, P, size):
    """Return a sample of random variates for a generic discrete probability function.

    This is a vectorized version of the cumulative distribution method, where:
    * n=size random numbers in [0, 1] are extracted;
    * they are digitized according to the cumulative distribution of the distribution;
    * the digitized indices are mapped back into the k values.
    """
    return np.take(k, np.digitize(np.random.random(size), np.cumsum(P)))


def k2_prob_dist(mean, fano_factor):
    """Return the "frozen" probability distribution for the two-k case
    mu < 1. - F, i.e., k = [0, 1] and:
    * P(0) = 1. - mu
    * P(1) = mu
    """
    assert mean >= 0 and mean <= 1. - fano_factor
    k = np.array([0, 1])
    P = np.array([1. - mean, mean])
    assert (P >= 0).all()
    return k, P


def k2_rv(mean, fano_factor, size):
    """Random variate for the k2 distribution.
    """
    return _discrete_random_variate(*k2_prob_dist(mean, fano_factor), size)


def k3_prob_dist(mean, fano_factor):
    """Return the "frozen" probability distribution for the three-k case
    1 - F <= mu < 2. - F, i.e., k = [0, 1, 2] and:
    * P(0) = 1. - 0.5 * mu * (2. - f)
    * P(1) = mu * (1. - f)
    * P(2) = 0.5 * mu * f
    where f = mu + F  - 1.
    """
    assert mean >= 1. - fano_factor and mean <= 2. - fano_factor and fano_factor >= CRITICAL_FANO
    f = mean + fano_factor - 1.
    k = np.array([0, 1, 2])
    P = np.array([1. - 0.5 * mean * (2. - f), mean * (1. - f), 0.5 * mean * f])
    assert (P >= 0).all()
    return k, P


def k3_rv(mean, fano_factor, size):
    """Random variate for the k3 distribution.
    """
    return _discrete_random_variate(*k3_prob_dist(mean, fano_factor), size)


def _gen_poisson_params(mean, fano_factor):
    """Return the theta and lambda parameters of a generalized Poisson
    distribution, given the desired mean and Fano factors.

    This is a trivial inversion of the formulae for the mean and variance of
    the distribution.
    """
    lambda_ = 1. - 1. / np.sqrt(fano_factor)
    assert lambda_ < 0
    theta = mean * (1 - lambda_)
    kmax = -int(theta / lambda_)
    return theta, lambda_, kmax


def _gen_poisson_prob(k, theta, lambda_):
    """Generalized Poisson probability.
    """
    return theta * np.power(theta + lambda_ * k, k - 1.) * np.exp(-theta - lambda_ * k) / factorial(k)


def gen_poisson_prob_dist(mean, fano_factor, normalize=False):
    """Generalized Poisson distribution
    """
    theta, lambda_, kmax = _gen_poisson_params(mean, fano_factor)
    k = np.arange(0, kmax + 1)
    P = _gen_poisson_prob(k, theta, lambda_)
    if normalize:
        P /= P.sum()
    return k, P

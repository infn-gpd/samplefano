#!/usr/bin/env python
#
# Copyright (C) 2020, the IXPE team.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


"""matplotlib configuration module.
"""

import matplotlib
import os
import sys

from matplotlib import pyplot as plt

if sys.flags.interactive:
    plt.ion()




DEFAULT_FIG_WIDTH = 8.

DEFAULT_FIG_HEIGHT = 6.

DEFAULT_FIG_SIZE = (DEFAULT_FIG_WIDTH, DEFAULT_FIG_HEIGHT)

DEFAULT_COLORS = [
    '#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b',
    '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'
]


SERIF_FONTS = [
    'DejaVu Serif', 'Bitstream Vera Serif', 'New Century Schoolbook',
    'Century Schoolbook L', 'Utopia', 'ITC Bookman', 'Bookman',
    'Nimbus Roman No9 L', 'Times New Roman', 'Times', 'Palatino', 'Charter',
    'serif'
]

SANS_SERIF_FONTS = [
    'DejaVu Sans', 'Bitstream Vera Sans', 'Lucida Grande', 'Verdana', 'Geneva',
    'Lucid', 'Arial', 'Helvetica', 'Avant Garde', 'sans-serif'
]

CURSIVE_FONTS = [
    'Apple Chancery', 'Textile', 'Zapf Chancery', 'Sand', 'Script MT',
    'Felipa', 'cursive'
]

FANTASY_FONTS = [
    'Comic Sans MS', 'Chicago', 'Charcoal', 'Impact', 'Western', 'Humor Sans',
    'xkcd', 'fantasy'
]

MONOSPACE_FONTS = [
    'DejaVu Sans Mono', 'Bitstream Vera Sans Mono', 'Andale Mono',
    'Nimbus Mono L', 'Courier New', 'Courier', 'Fixed', 'Terminal', 'monospace'
]


def save_gcf(output_folder='.', file_extensions=['pdf', 'png']):
    """Save the current matplotlib figure.

    If the current figure has a sensible name, it will be used to construct
    the path to the output file---we just make everything lower case and
    replace spaces with `_`.

    Arguments
    ---------
    output_folder : string
        The path to the output folder (default to pwd).

    file_extensions : list of strings
        A list of extensions the figure needs to be saved into.

    Returns
    -------
    list
        The list of paths to the file(s) being created by the function.
    """
    file_list = []
    file_base = plt.gcf().get_label().lower().replace(' ', '_')
    assert file_base != ''
    for ext in file_extensions:
        file_name = '%s.%s' % (file_base, ext)
        file_path = os.path.join(output_folder, file_name)
        print('Saving current figure to %s...' % file_path)
        try:
            plt.savefig(file_path, transparent=False)
            file_list.append(file_path)
        except IOError as e:
            print(e)
    return file_list


def save_all_figures(output_folder='.', file_extensions=['pdf', 'png']):
    """Save all the figures in memory to a given output folder.
    """
    print('Looping over matplotlib figures and saving all of them...')
    for figid in plt.get_fignums():
        plt.figure(figid)
        save_gcf(output_folder, file_extensions=file_extensions)


def residual_plot(figure_name=None, separation=0.3, padding=0.01,
                  ylabel_offset=-0.085):
    """Create a new figure with two axes objects for residual plots.

    Arguments
    ---------
    figure_name : str
        The name of the figure.

    separation : float
        The vertical separation point between the two axes.
    """
    left = rc_param('figure.subplot.left')
    right = rc_param('figure.subplot.right')
    bot = rc_param('figure.subplot.bottom')
    top = rc_param('figure.subplot.top')
    fig = plt.figure(figure_name)
    ax1 = fig.add_axes((left, separation + padding, right - left,
                        top - separation))
    ax1.set_xticklabels([])
    ax1.get_yaxis().set_label_coords(ylabel_offset, 0.5)
    ax2 = fig.add_axes((left, bot, right - left, separation - bot - padding))
    plt.sca(ax1)
    ax2.get_yaxis().set_label_coords(ylabel_offset, 0.5)
    return ax1, ax2


def last_line_color(default='black'):
    """Return the color used to draw the last line
    """
    try:
        return plt.gca().get_lines()[-1].get_color()
    except:
        return default


def marker(x, y, **kwargs):
    """Draw a marker at a specified point.
    """
    plt.plot([x], [y], 'o', **kwargs)


def labeled_marker(x, y, label, dx=0, dy=0, **kwargs):
    """Draw a marker and a label at a specified point.
    """
    if kwargs.get('color') is None:
        kwargs['color'] = last_line_color()
    marker(x, y, color=kwargs.get('color'))
    plt.text(x + dx, y + dy, label, **kwargs)


def text_box(x, y, *lines, **kwargs):
    """
    """
    boxstyle = dict(boxstyle='round', facecolor='white', alpha=0.75)
    plt.text(x, y, '\n'.join(lines), transform=plt.gca().transAxes, bbox=boxstyle, **kwargs)


def setup_gca(xlabel=None, ylabel=None, xmin=None, xmax=None, ymin=None,
              ymax=None, logx=False, logy=False, grids=False, xticks=None,
              yticks=None, legend=False):
    """Setup the axes for the current plot.
    """
    if logx is True:
        plt.xscale('log')
    if logy is True:
        plt.yscale('log')
    if xticks is not None:
        plt.gca().set_xticks(xticks)
    if yticks is not None:
        plt.gca().set_yticks(yticks)
    if xlabel is not None:
        plt.xlabel(xlabel)
    if ylabel is not None:
        plt.ylabel(ylabel)
    if xmin is None and xmax is None and ymin is None and ymax is None:
        pass
    else:
        plt.axis([xmin, xmax, ymin, ymax])
    if grids:
        plt.grid(which='both')
    if legend:
        plt.legend()


def annotation(text, pos, text_pos, color='black', text_size=13.,
               style='angle,angleA=0,angleB=90,rad=10', ax=None):
    """Annotate the current figure.
    """
    _arrowprops = dict(arrowstyle="->", #head_length=0.5, head_width=0.25,
                       color=color, connectionstyle=style)
    if ax is None:
        ax = plt.gca()
    ax.annotate(text, xy=pos, xycoords='data', xytext=text_pos, textcoords='data',
                size=text_size, va='center', ha='center', color=color, arrowprops=_arrowprops)



class ixpeStatBox:

    """Base class describing a text box, to be used for the histogram and fit
    stat boxes.

    In the initial implementation this was wrapped into a simple function,
    but we immediately run into problems in cases where we needed to
    setup a stat box in differen steps before drawing it on the canvas
    (e.g., when a FitModel subclass needs to customize the stat box of the
    base class). The class approach is more flexible, although one needs
    a few more lines of code to add entries and plot the thing.

    Parameters
    ----------
    position : str of tuple
        It can either be a two-element tuple (in which case the argument is
        interpreted as a position in absolute coordinates, with the reference
        corner determined by the alignment flags), or a string in the
        set ['upper left', 'upper right', 'lower left', 'lower rigth'].
        If position is a string, the alignment flags are ignored.

    halign : str
        The horizontal alignment ('left' | 'center' | 'right')

    valign : str
        The vertical alignment ('top' | 'center' | 'bottom')
    """

    HORIZONTAL_PADDING = 0.025
    VERTICAL_PADDING = 0.035
    _left, _right = HORIZONTAL_PADDING, 1 - HORIZONTAL_PADDING
    _bottom, _top = VERTICAL_PADDING, 1 - VERTICAL_PADDING
    POSITION_DICT = {
        'upper left': (_left, _top, 'left', 'top'),
        'upper right': (_right, _top, 'right', 'top'),
        'lower left': (_left, _bottom, 'left', 'bottom'),
        'lower right': (_right, _bottom, 'right', 'bottom')
    }
    DEFAULT_BBOX = dict(boxstyle='round', facecolor='white', alpha=0.75)

    def __init__(self, position='upper left', halign='left', valign='top'):
        """Constructor.
        """
        self.set_position(position, halign, valign)
        self.text = ''

    def set_position(self, position, halign='left', valign='top'):
        """Set the position of the bounding box.
        """
        if isinstance(position, str):
            self.x0, self.y0, self.halign,\
                self.valign = self.POSITION_DICT[position]
        else:
            self.x0, self.y0 = position
            self.halign, self.valign = halign, valign

    def add_entry(self, label, value=None, error=None):
        """Add an entry to the stat box.
        """
        if value is None and error is None:
            self.text += '%s\n' % label
        elif value is not None and error is None:
            self.text += '%s: %s\n' % (label, format_value(value))
        elif value is not None and error is not None:
            self.text += '%s: %s\n' %\
                         (label, format_value_error(value, error, pm='$\\pm$'))

    def plot(self, **kwargs):
        """Plot the stat box.

        Parameters
        ----------
        **kwargs : dict
            The options to be passed to `matplotlib.pyplot.text()`
        """
        def set_kwargs_default(key, value):
            """
            """
            if key not in kwargs:
                kwargs[key] = value

        set_kwargs_default('horizontalalignment', self.halign)
        set_kwargs_default('verticalalignment', self.valign)
        set_kwargs_default('bbox', self.DEFAULT_BBOX)
        set_kwargs_default('transform', plt.gca().transAxes)
        plt.text(self.x0, self.y0, self.text.strip('\n'), **kwargs)



def rc_param(key):
    """Return a given matplotlib configuration property.
    """
    return matplotlib.rcParams[key]


def _set_rc_param(key, value):
    """Set the value for a single matplotlib parameter.

    The actual command is encapsulated into a try except block because this
    is intended to work across different matplotlib versions. If a setting
    cannot be applied for whatever reason, this will happily move on.
    """
    #logger.debug('Setting %s to %s' % (key, value))
    try:
        matplotlib.rcParams[key] = value
    except Exception as e:
        #logger.debug('Cannot apply setting (%s).' % e)
        pass


def setup():
    """Basic system-wide setup.

    The vast majority of the settings are taken verbatim from the
    matplotlib 2, commit 5285e76:
    https://github.com/matplotlib/matplotlib/blob/master/matplotlibrc.template

    Note that, although this is designed to provide an experience which is as
    consistent as possible across different matplotlib versions, some of the
    functionalities are not implemented in older versions, which is why we wrap
    each parameter setting into a _set_rc_param() function call.
    """
    # http://matplotlib.org/api/artist_api.html#module-matplotlib.lines
    _set_rc_param('lines.linewidth', 1.5)
    _set_rc_param('lines.linestyle', '-')
    _set_rc_param('lines.color', DEFAULT_COLORS[0])
    _set_rc_param('lines.marker', None)
    _set_rc_param('lines.markeredgewidth', 1.0)
    _set_rc_param('lines.markersize', 6)
    _set_rc_param('lines.dash_joinstyle', 'miter')
    _set_rc_param('lines.dash_capstyle', 'butt')
    _set_rc_param('lines.solid_joinstyle', 'miter')
    _set_rc_param('lines.solid_capstyle', 'projecting')
    _set_rc_param('lines.antialiased', True)
    _set_rc_param('lines.dashed_pattern', (2.8, 1.2))
    _set_rc_param('lines.dashdot_pattern', (4.8, 1.2, 0.8, 1.2))
    _set_rc_param('lines.dotted_pattern', (1.1, 1.1))
    _set_rc_param('lines.scale_dashes', True)

    # Markers.
    _set_rc_param('markers.fillstyle', 'full')

    # http://matplotlib.org/api/artist_api.html#module-matplotlib.patches
    _set_rc_param('patch.linewidth', 1)
    _set_rc_param('patch.facecolor', DEFAULT_COLORS[0])
    _set_rc_param('patch.edgecolor', 'black')
    _set_rc_param('patch.force_edgecolor', True)
    _set_rc_param('patch.antialiased', True)

    # Hatches
    _set_rc_param('hatch.color', 'k')
    _set_rc_param('hatch.linewidth', 1.0)

    # Boxplot
    _set_rc_param('boxplot.notch', False)
    _set_rc_param('boxplot.vertical', True)
    _set_rc_param('boxplot.whiskers', 1.5)
    _set_rc_param('boxplot.bootstrap', None)
    _set_rc_param('boxplot.patchartist', False)
    _set_rc_param('boxplot.showmeans', False)
    _set_rc_param('boxplot.showcaps', True)
    _set_rc_param('boxplot.showbox', True)
    _set_rc_param('boxplot.showfliers', True)
    _set_rc_param('boxplot.meanline', False)
    _set_rc_param('boxplot.flierprops.color', 'k')
    _set_rc_param('boxplot.flierprops.marker', 'o')
    _set_rc_param('boxplot.flierprops.markerfacecolor', 'none')
    _set_rc_param('boxplot.flierprops.markeredgecolor', 'k')
    _set_rc_param('boxplot.flierprops.markersize', 6)
    _set_rc_param('boxplot.flierprops.linestyle', 'none')
    _set_rc_param('boxplot.flierprops.linewidth', 1.0)
    _set_rc_param('boxplot.boxprops.color', 'k')
    _set_rc_param('boxplot.boxprops.linewidth', 1.0)
    _set_rc_param('boxplot.boxprops.linestyle', '-')
    _set_rc_param('boxplot.whiskerprops.color', 'k')
    _set_rc_param('boxplot.whiskerprops.linewidth', 1.0)
    _set_rc_param('boxplot.whiskerprops.linestyle', '-')
    _set_rc_param('boxplot.capprops.color', 'k')
    _set_rc_param('boxplot.capprops.linewidth', 1.0)
    _set_rc_param('boxplot.capprops.linestyle', '-')
    _set_rc_param('boxplot.medianprops.color', DEFAULT_COLORS[1])
    _set_rc_param('boxplot.medianprops.linewidth', 1.0)
    _set_rc_param('boxplot.medianprops.linestyle', '-')
    _set_rc_param('boxplot.meanprops.color', DEFAULT_COLORS[2])
    _set_rc_param('boxplot.meanprops.marker', '^')
    _set_rc_param('boxplot.meanprops.markerfacecolor', DEFAULT_COLORS[2])
    _set_rc_param('boxplot.meanprops.markeredgecolor', DEFAULT_COLORS[2])
    _set_rc_param('boxplot.meanprops.markersize', 6)
    _set_rc_param('boxplot.meanprops.linestyle', 'none')
    _set_rc_param('boxplot.meanprops.linewidth', 1.0)

    # http://matplotlib.org/api/font_manager_api.html for more
    _set_rc_param('font.family', 'sans-serif')
    _set_rc_param('font.style', 'normal')
    _set_rc_param('font.variant', 'normal')
    _set_rc_param('font.weight', 'medium')
    _set_rc_param('font.stretch', 'normal')
    _set_rc_param('font.size', 14.0)
    _set_rc_param('font.serif', SERIF_FONTS)
    _set_rc_param('font.sans-serif', SANS_SERIF_FONTS)
    _set_rc_param('font.cursive', CURSIVE_FONTS)
    _set_rc_param('font.fantasy', FANTASY_FONTS)
    _set_rc_param('font.monospace', MONOSPACE_FONTS)

    # http://matplotlib.org/api/artist_api.html#module-matplotlib.text for more
    _set_rc_param('text.color', 'black')

    #http://wiki.scipy.org/Cookbook/Matplotlib/UsingTex
    _set_rc_param('text.usetex', False)
    _set_rc_param('text.hinting', 'auto')
    _set_rc_param('text.hinting_factor', 8)
    _set_rc_param('text.antialiased', True)
    _set_rc_param('mathtext.cal', 'cursive')
    _set_rc_param('mathtext.rm', 'serif')
    _set_rc_param('mathtext.tt', 'monospace')
    _set_rc_param('mathtext.it', 'serif:italic')
    _set_rc_param('mathtext.bf', 'serif:bold')
    _set_rc_param('mathtext.sf', 'sans')
    _set_rc_param('mathtext.fontset', 'stixsans')
    _set_rc_param('mathtext.fallback_to_cm', True)
    _set_rc_param('mathtext.default', 'it')

    # http://matplotlib.org/api/axes_api.html#module-matplotlib.axes
    _set_rc_param('axes.facecolor', 'white')
    _set_rc_param('axes.edgecolor', 'black')
    _set_rc_param('axes.linewidth', 1.25)
    _set_rc_param('axes.grid', False)
    _set_rc_param('axes.titlesize', 'large')
    _set_rc_param('axes.titlepad', 6.0)
    _set_rc_param('axes.labelsize', 'medium')
    _set_rc_param('axes.labelpad', 4.0)
    _set_rc_param('axes.labelweight', 'normal')
    _set_rc_param('axes.labelcolor', 'black')
    _set_rc_param('axes.axisbelow', 'line')
    _set_rc_param('axes.formatter.limits', (-7, 7))
    _set_rc_param('axes.formatter.use_locale', False)
    _set_rc_param('axes.formatter.use_mathtext', False)
    _set_rc_param('axes.formatter.min_exponent', 0)
    _set_rc_param('axes.formatter.useoffset', True)
    _set_rc_param('axes.formatter.offset_threshold', 4)
    _set_rc_param('axes.spine.left', True)
    _set_rc_param('axes.spine.bottom', True)
    _set_rc_param('axes.spine.top', True)
    _set_rc_param('axes.spine.right', True)
    _set_rc_param('axes.unicode_minus', True)
    _set_rc_param('axes.autolimit_mode', 'round_numbers')
    _set_rc_param('axes.xmargin', 0.)
    _set_rc_param('axes.ymargin', 0.)
    _set_rc_param('polaraxes.grid', True)
    _set_rc_param('axes3d.grid', True)

    # Dates
    _set_rc_param('date.autoformatter.year', '%Y')
    _set_rc_param('date.autoformatter.month', '%Y-%m')
    _set_rc_param('date.autoformatter.day', '%Y-%m-%d')
    _set_rc_param('date.autoformatter.hour', ' %m-%d %H')
    _set_rc_param('date.autoformatter.minute', '%d %H:%M')
    _set_rc_param('date.autoformatter.second', '%H:%M:%S')
    _set_rc_param('date.autoformatter.microsecond', '%M:%S.%f')

    # see http://matplotlib.org/api/axis_api.html#matplotlib.axis.Tick
    _set_rc_param('xtick.top', False)
    _set_rc_param('xtick.bottom', True)
    _set_rc_param('xtick.major.size', 3.5)
    _set_rc_param('xtick.minor.size', 2)
    _set_rc_param('xtick.major.width', 1.25)
    _set_rc_param('xtick.minor.width', 1.)
    _set_rc_param('xtick.major.pad', 3.5)
    _set_rc_param('xtick.minor.pad', 3.4)
    _set_rc_param('xtick.color', 'k')
    _set_rc_param('xtick.labelsize', 'medium')
    _set_rc_param('xtick.direction', 'out')
    _set_rc_param('xtick.minor.visible', False)
    _set_rc_param('xtick.major.top', True)
    _set_rc_param('xtick.major.bottom', True)
    _set_rc_param('xtick.minor.top', True)
    _set_rc_param('xtick.minor.bottom', True)
    _set_rc_param('ytick.left', True)
    _set_rc_param('ytick.right', False)
    _set_rc_param('ytick.major.size', 3.5)
    _set_rc_param('ytick.minor.size', 2)
    _set_rc_param('ytick.major.width', 1.25)
    _set_rc_param('ytick.minor.width', 1.)
    _set_rc_param('ytick.major.pad', 3.5)
    _set_rc_param('ytick.minor.pad', 3.4)
    _set_rc_param('ytick.color', 'k')
    _set_rc_param('ytick.labelsize', 'medium')
    _set_rc_param('ytick.direction', 'out')
    _set_rc_param('ytick.minor.visible', False)
    _set_rc_param('ytick.major.left', True)
    _set_rc_param('ytick.major.right', True)
    _set_rc_param('ytick.minor.left', True)
    _set_rc_param('ytick.minor.right', True)

    # Grids
    _set_rc_param('grid.color', '#c0c0c0')
    _set_rc_param('grid.linestyle', '--')
    _set_rc_param('grid.linewidth', 0.8)
    _set_rc_param('grid.alpha', 1.0)

    # Legend
    _set_rc_param('legend.loc', 'best')
    _set_rc_param('legend.frameon', True)
    _set_rc_param('legend.framealpha', 0.8)
    _set_rc_param('legend.facecolor', 'inherit')
    _set_rc_param('legend.edgecolor', 0.8)
    _set_rc_param('legend.fancybox', True)
    _set_rc_param('legend.shadow', False)
    _set_rc_param('legend.numpoints', 1)
    _set_rc_param('legend.scatterpoints', 1)
    _set_rc_param('legend.markerscale', 1.0)
    _set_rc_param('legend.fontsize', 'medium')
    _set_rc_param('legend.borderpad', 0.4)
    _set_rc_param('legend.labelspacing', 0.5)
    _set_rc_param('legend.handlelength', 2.0)
    _set_rc_param('legend.handleheight', 0.7)
    _set_rc_param('legend.handletextpad', 0.8)
    _set_rc_param('legend.borderaxespad', 0.5)
    _set_rc_param('legend.columnspacing', 2.0)

    # See http://matplotlib.org/api/figure_api.html#matplotlib.figure.Figure
    _set_rc_param('figure.titlesize', 'large')
    _set_rc_param('figure.titleweight', 'normal')
    _set_rc_param('figure.figsize', DEFAULT_FIG_SIZE)
    _set_rc_param('figure.dpi', 100)
    _set_rc_param('figure.facecolor', 'white')
    _set_rc_param('figure.edgecolor', 'white')
    _set_rc_param('figure.autolayout', False)
    _set_rc_param('figure.max_open_warning', 20)
    _set_rc_param('figure.subplot.left', 0.125)
    _set_rc_param('figure.subplot.right', 0.95)
    _set_rc_param('figure.subplot.bottom', 0.10)
    _set_rc_param('figure.subplot.top', 0.95)
    _set_rc_param('figure.subplot.wspace', 0.2)
    _set_rc_param('figure.subplot.hspace', 0.2)

    # Images
    _set_rc_param('image.aspect', 'equal')
    _set_rc_param('image.interpolation', 'nearest')
    _set_rc_param('image.cmap', 'jet')
    _set_rc_param('image.lut', 256)
    _set_rc_param('image.origin', 'upper')
    _set_rc_param('image.resample', True)
    _set_rc_param('image.composite_image', True)

    # Contour plots
    _set_rc_param('contour.negative_linestyle', 'dashed')
    _set_rc_param('contour.corner_mask', True)

    # Errorbar plots
    _set_rc_param('errorbar.capsize', 0)

    # Histogram plots
    _set_rc_param('hist.bins', 10)

    # Scatter plots
    _set_rc_param('scatter.marker', 'o')

    # Saving figures
    _set_rc_param('path.simplify', True)
    _set_rc_param('path.simplify_threshold', 0.1)
    _set_rc_param('path.snap', True)
    _set_rc_param('path.sketch', None)
    _set_rc_param('savefig.dpi', 'figure')
    _set_rc_param('savefig.facecolor', 'white')
    _set_rc_param('savefig.edgecolor', 'white')
    _set_rc_param('savefig.format', 'png')
    _set_rc_param('savefig.bbox', 'standard')
    _set_rc_param('savefig.pad_inches', 0.1)
    _set_rc_param('savefig.jpeg_quality', 95)
    _set_rc_param('savefig.directory', '~')
    _set_rc_param('savefig.transparent', False)

    # Back-ends
    _set_rc_param('pdf.compression', 6)
    _set_rc_param('pdf.fonttype', 3)
    _set_rc_param('svg.image_inline', True)
    _set_rc_param('svg.fonttype', 'path')
    _set_rc_param('svg.hashsalt', None)

    # Key maps
    _set_rc_param('keymap.fullscreen', ('f', 'ctrl+f'))
    _set_rc_param('keymap.home', ('h', 'r', 'home'))
    _set_rc_param('keymap.back', ('left', 'c', 'backspace'))
    _set_rc_param('keymap.forward', ('right', 'v'))
    _set_rc_param('keymap.pan', 'p')
    _set_rc_param('keymap.zoom', 'o')
    _set_rc_param('keymap.save', 's')
    _set_rc_param('keymap.quit', ('ctrl+w', 'cmd+w'))
    _set_rc_param('keymap.grid', 'g')
    _set_rc_param('keymap.grid_minor', 'G')
    _set_rc_param('keymap.yscale', 'l')
    _set_rc_param('keymap.xscale', ('L', 'k'))
    _set_rc_param('keymap.all_axes', 'a')

    # Animations settings
    _set_rc_param('animation.html', 'none')
    _set_rc_param('animation.writer', 'ffmpeg')
    _set_rc_param('animation.codec', 'h264')
    _set_rc_param('animation.bitrate', -1)
    _set_rc_param('animation.frame_format', 'png')
    _set_rc_param('animation.html_args', '')
    _set_rc_param('animation.ffmpeg_path', 'ffmpeg')
    _set_rc_param('animation.ffmpeg_args', '')
    _set_rc_param('animation.avconv_path', 'avconv')
    _set_rc_param('animation.avconv_args', '')
    _set_rc_param('animation.mencoder_path', 'mencoder')
    _set_rc_param('animation.mencoder_args', '')
    _set_rc_param('animation.convert_path', 'convert')

    # Miscellanea
    _set_rc_param('timezone', 'UTC')


setup()

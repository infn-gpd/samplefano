# Copyright (C) 2021, Luca Baldini.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from matplotlib_ import plt, setup_gca, last_line_color, save_gcf
from scipy.special import factorial


def generalized_poisson_pdf(k, theta, lambda_):
    """Generalized Poisson distribution.
    """
    return theta * np.power(theta + lambda_ * k, k - 1.) * np.exp(-theta - lambda_ * k) / factorial(k)


def generalized_poisson_parameters(mu, fano_factor):
    """Return the theta and lambda parameters of a generalized Poisson
    distribution, given the desidered mean and Fano factors.

    This is a trivial inversion of the formulae for the mean and variance of
    the distribution.
    """
    lambda_ = 1. - 1. / np.sqrt(fano_factor)
    assert lambda_ < 0
    theta = mu * (1 - lambda_)
    kmax = -int(theta / lambda_)
    return theta, lambda_, kmax


def generalized_poisson_variate(mu,F,d_norm,num_method):
    """Sample the generalized Poisson distribution. d_norm is different from zero only if needed to correct the normalization. num_method identifies the type of correction wanted:
    num_method=0 => correcting the normalization adding to every  probability an equal amount.
    num_method!=0 => adding what lacks in the normalization to the biggest probability in order to modify less possible the mean
    """
    u = np.random.uniform(0., 1.)
    if mu < 1. - F:
        if u >= mu:
            return 0
        return 1
    if mu < 2. - F:
        f = mu + F - 1.
        cdf = 1.- 0.5 * mu * (2. - f)
        if u <= cdf:
            return 0
        cdf += mu * (1. - f)
        if u <= cdf:
            return 1
        return 2
    lambda_ = 1. - 1. / np.sqrt(F)
    theta = mu * (1 - lambda_)
    kmax = -int(theta / lambda_)
    P = np.exp(-theta)
    cdf=P
    k = 0
    delta = np.exp(-lambda_)
    ##First method: correcting the normalization adding to every  probability an equal amount (if no correction is wanted, d_norm=0)

    if(num_method==0):
        cdf+=d_norm
        while cdf < u:
            k += 1
            if k == kmax:
                return k
            P *= (delta / k) * ((theta + lambda_ * k)**(k - 1.) / (theta + lambda_ * (k - 1.))**(k - 2.))
            cdf+=(P+d_norm)
        return k
    ##Secondo method: adding what lacks in the normalization to the biggest probability in order to modify less possible the mean
    else:
        m=np.rint(mu)
        while cdf < u:
            k += 1
            if k == kmax:
                return k
            P *= (delta / k) * ((theta + lambda_ * k)**(k - 1.) / (theta + lambda_ * (k - 1.))**(k - 2.))
            if(m==k):
                cdf+=(P+d_norm)
            else:
                cdf+=P
        return k


def gamma_variate(mu, fano_factor, size):
    """
    """
    shape = mu / fano_factor
    scale = fano_factor
    return (np.random.gamma(shape, scale, size) + 0.5).astype(int)


def plot(fano_factor, size=10000):
    """
    """
    mu = np.linspace(2.-fano_factor, 5., 50)
    norm = []
    mean = []
    variance = []
    kmax = []
    ##Arrays for first method:no correction on norm
    var_mean = []
    var_variance = []
    gammavar_mean = []
    gammavar_variance = []
    ##Arrays for second method: correcting norm with homogeneous add
    delta_norm = []
    norm_b=[]
    mean_b=[]
    variance_b=[]
    var_mean_b=[]
    var_variance_b=[]
    gammavar_mean_b = []
    gammavar_variance_b = []
    ##Arrays for third method: correcting norm adding the lack on the peak
    norm_c=[]
    mean_c=[]
    variance_c=[]
    var_mean_c=[]
    var_variance_c=[]
    gammavar_mean_c = []
    gammavar_variance_c = []
    delta_norm_c= []

    ind=0

    for _mu in mu:
        theta, lambda_, _kmax = generalized_poisson_parameters(_mu, fano_factor)
        #print(_mu, theta, lambda_,_kmax)
        kmax.append(_kmax)
        k = np.arange(0, _kmax)
        P = generalized_poisson_pdf(k, theta, lambda_)
        #print(P)
        norm.append(P.sum())

        #Defining the lack of the normalization
        d_norm=(1-norm[ind])/_kmax
        delta_norm.append(d_norm)
        d_norm_tot=1-norm[ind]
        delta_norm_c.append(d_norm_tot)
        ##Sampling without any correction on normalization
        _mean = (k * P).sum()
        mean.append(_mean)
        variance.append(((k - _mean)**2. * P).sum())
        x = np.array([generalized_poisson_variate(_mu, fano_factor,0,0) for i in range(size)])
        var_mean.append(x.mean())
        var_variance.append(x.std()**2.)
        x = gamma_variate(_mu, fano_factor, size)
        gammavar_mean.append(x.mean())
        gammavar_variance.append(x.std()**2.)


    ##Correcting the normalization, method 1: correcting the normalization adding to every  probability an equal amount
        P+=d_norm
        norm_b.append(P.sum())
        _mean = (k * P).sum()
        mean_b.append(_mean)
        variance_b.append(((k - _mean)**2. * P).sum())
        x = np.array([generalized_poisson_variate(_mu, fano_factor,d_norm,0) for i in range(size)])
        var_mean_b.append(x.mean())
        var_variance_b.append(x.std()**2.)
        x = gamma_variate(_mu, fano_factor, size)
        #print(x.mean())
        gammavar_mean.append(x.mean())
        gammavar_variance.append(x.std()**2.)

        P-=d_norm

    ##Correcting the normalization, method 2: adding what lacks in the normalization to the biggest probability in order to modify less possible the mean
        m=np.rint(_mu)
        m=int(m)
        if(m==0):
            P[0]+=d_norm_tot
        else:
            P[m]+=d_norm_tot
        norm_c.append(P.sum())
        #_mean = (k * P).sum()
        #mean_b.append(_mean)
        #variance_b.append(((k - _mean)**2. * P).sum())
        x = np.array([generalized_poisson_variate(_mu, fano_factor,d_norm_tot,1) for i in range(size)])
        var_mean_c.append(x.mean())
        var_variance_c.append(x.std()**2.)
        #x = gamma_variate(_mu, fano_factor, size)
        #print(x.mean())
        #gammavar_mean.append(x.mean())
        #gammavar_variance.append(x.std()**2.)

        ind+=1

    ##Summary plot: comparing the results of the three methods in sampled mean, normalization and sampled variance

    fig,((ax1,ax2,ax3)) = plt.subplots(3, 1)
    fig.suptitle('Mean,variance,sum of probabilities with $F=%ls$'% fano_factor)
    ones=np.ones(len(var_mean))
    ##sample mean - expected mean plot
    ax1.axvline(1. - fano_factor, ls='dashed', color='black')
    ax1.axvline(2. - fano_factor, ls='dashed', color='black')
    ax1.errorbar(mu, var_mean / mu - ones, fmt='o',label=r'method 1',color='green')
    ax1.errorbar(mu, var_mean_b / mu - ones, fmt='v',label=r'method 2',color='red')
    ax1.errorbar(mu, var_mean_c / mu - ones, fmt='+',label=r'method 3',color='blue')
    ax1.axhline(0., ls='dashed', color='black')
    ax1.set(xlabel=r'$\left<n\right>$',ylabel=r'$\frac{\mu_{sampled}}{\mu_{expected}}-1$')

    #plt.text(2., 1.05, r'Sample mean / $\left<n\right>$')

    ##sum of probabilities - 1

    ax2.axvline(1. - fano_factor, ls='dashed', color='black')
    ax2.axvline(2. - fano_factor, ls='dashed', color='black')
    ax2.axhline(0., ls='dashed', color='black')
    ax2.errorbar(mu, norm-ones, fmt='o',color='green')
    ax2.errorbar(mu, norm_b-ones, fmt='v',color='red')
    ax2.errorbar(mu, norm_c-ones, fmt='+',color='blue')
    ax2.set(xlabel=r'$\left<n\right>$',ylabel=r'sum of P - 1')

    ##sample variance - expected variance plot
    __y=(ones - mu) * (mu <= 1. - fano_factor) + fano_factor * (mu > 1. - fano_factor) #expected variance
    ax3.axvline(1. - fano_factor, ls='dashed', color='black')
    ax3.axvline(2. - fano_factor, ls='dashed', color='black')
    ax3.errorbar(mu, var_variance / mu - __y, fmt='o',color='green')
    ax3.errorbar(mu, var_variance_b / mu - __y, fmt='v',color='red')
    ax3.errorbar(mu, var_variance_c / mu - __y, fmt='+',color='blue')
    ax3.axhline(0., ls='dashed', color='black')
    ax3.set(xlabel=r'$\left<n\right>$',ylabel=r'$\frac{\sigma_{sampled}}{\sigma_{expected}}-1$')
    #plt.errorbar(mu, var_variance / mu, fmt='o',label=r'Sample variance / $\left<n\right>$ for F=%2f'%fano_factor)
    #plt.text(2., 0.30, r'Sample variance / $\left<n\right>$')

    fig.legend(loc='best')


    ##limit of the three regimes lines

    #plt.plot(mu, norm)
    #setup_gca(xlabel=r'$\left<n\right>$', grids=True, ymin=-0.1, ymax=0.1,
              #ylabel=r'Somma delle P -1 (F=%.2f)'%fano_factor)

    #save_gcf()



    ##3D plot: mu vs sampled mu vs Fano factor
    '''
    fig2 = plt.figure('GPD')
    _y=(1. - mu) * (mu <= 1. - fano_factor) + fano_factor * (mu > 1. - fano_factor) #expected variance
    ax = fig2.gca(projection='3d')
    ax.plot(mu, var_mean / mu - ones, fano_factor, marker='.',linestyle='')

    #ax.plot(mu, 100*np.abs(var_variance-_y*mu)/(_y*mu), fano_factor, marker='.',linestyle='')
    #x1=np.linspace(min(mu),max(mu),1000)
    #y1=np.ones(len(x1))
    _x = np.linspace(0., mu.max(), 250)
    _y = (1. - _x) * (_x <= 1. - fano_factor) + fano_factor * (_x > 1. - fano_factor)
    #plt.plot(_x, _y,zs=fano_factor, ls='dashed', color=last_line_color())
    #ax.plot(x1,y1,zs=fano_factor,ls='dashed', color=last_line_color())
    #plt.axhline(1., ls='dashed', color=last_line_color())

    ax.set_xlabel(r'$\left<n\right>$')
    ax.set_ylabel(r'$\frac{\mu_{sampled}}{\mu_{expected}}-1$')
    ax.set_zlabel('Fano factor F')
    '''

##Plotting sampled mu and sampled variance for a chosen Fano factor
    '''
    plt.figure('Generalized Poisson')
    plt.errorbar(mu, var_mean / mu, fmt='o')
    #plt.errorbar(mu, gammavar_mean / mu, fmt='o', fillstyle='none', color=last_line_color())
    plt.axhline(1., ls='dashed', color=last_line_color())
    plt.text(2., 1.05, r'Sample mean / $\left<n\right>$')
    plt.errorbar(mu, var_variance / mu, fmt='o')
    #plt.errorbar(mu, gammavar_variance / mu, fmt='o', fillstyle='none', color=last_line_color())
    _x = np.linspace(0., mu.max(), 250)
    _y = (1. - _x) * (_x <= 1. - fano_factor) + fano_factor * (_x > 1. - fano_factor)
    plt.plot(_x, _y, ls='dashed', color=last_line_color())
    plt.text(2., 0.30, r'Sample variance / $\left<n\right>$')
    plt.axvline(1. - fano_factor, ls='dashed', color='black')
    plt.axvline(2. - fano_factor, ls='dashed', color='black')
    #plt.plot(mu, norm)
    setup_gca(xlabel=r'$\left<n\right>$', grids=True, ymin=0., ymax=1.25,
              ylabel='Generalized Poisson variate statistics (F = %.2f)' % fano_factor)
    save_gcf()
    '''

    """
    plt.figure('k max')
    plt.plot(mu, kmax)
    setup_gca(xlabel=r'$\left<n\right>$', ylabel='Generalized Poisson $k_{max}$', grids=True)
    plt.figure('Normalization')
    plt.plot(mu, norm)
    setup_gca(xlabel=r'$\left<n\right>$', ylabel='Generalized Poisson normalization', grids=True)
    plt.figure('Mean')
    plt.plot(mu, mean)
    plt.plot(mu, var_mean, 'o')
    plt.plot(mu, mu, ls='dashed')
    setup_gca(xlabel=r'$\left<n\right>$', ylabel='Generalized Poisson mean', grids=True)
    plt.figure('Variance')
    plt.plot(mu, variance)
    plt.plot(mu, var_variance, 'o')
    plt.plot(mu, mu * fano_factor, ls='dashed')
    setup_gca(xlabel=r'$\left<n\right>$', ylabel='Generalized Poisson variance', grids=True)
    """


if __name__ == '__main__':
    fano=np.linspace(0.18,0.9,10)
    for i in range(len(fano)):
        plot(fano[i])
        #plot_single()
    plt.show()
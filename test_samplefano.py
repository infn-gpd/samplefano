# Copyright (C) 2021, Luca Baldini.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


"""Unit tests for the samplefano module.
"""


import unittest
import sys

import numpy as np

import samplefano
from matplotlib_ import plt
if sys.flags.interactive:
    plt.ion()


class TestSampleFano(unittest.TestCase):

    """Unit test for the samplefano module.
    """

    def test_dist_stat(self):
        """Test the calculation of the basic statistics for a simple discrete
        distribution.
        """
        k = np.array([0, 1])
        P = np.array([0.5, 0.5])
        print(f'k = {k}, P = {P}')
        norm, mean, variance = samplefano.discrete_prob_dist_stat(k, P)
        print(f'Normalization = {norm}, mean = {mean}, variance = {variance}')
        self.assertAlmostEqual(norm, 1.)
        self.assertAlmostEqual(mean, 0.5)
        self.assertAlmostEqual(variance, 0.25)

    def test_random_variate(self, size=1000000):
        """Test the generic random variate generation routine for a simple
        discrete distribution.
        """
        k = np.array([0, 1])
        P = np.array([0.5, 0.5])
        print(f'k = {k}, P = {P}')
        rv = samplefano._discrete_random_variate(k, P, size)
        chisq, ndof, p_value = samplefano.discrete_chisquare_test(k, P, rv)
        print(f'Chisquare = {chisq} / {ndof} dof, P-value = {p_value}')
        self.assertTrue(p_value > 1.e-6)

    def test_k2_stats(self, fano_factor=samplefano.CRITICAL_FANO):
        """Test the normalization and mean of the two-state probability distribution.
        """
        print(f'Testing k2 for F={fano_factor}')
        for mu in np.linspace(0., 1. - fano_factor, 20):
            k, P = samplefano.k2_prob_dist(mu, fano_factor)
            norm, mean, variance = samplefano.discrete_prob_dist_stat(k, P)
            print(f'mu={mu:.4f}: {k} -> {P}')
            self.assertAlmostEqual(norm, 1.)
            self.assertAlmostEqual(mean, mu)

    def test_k2_random_variate(self, mean=0.5, fano_factor=samplefano.CRITICAL_FANO, size=1000000):
        """Test the k2 random variate.
        """
        print(f'Testing k2 random variate for mean={mean}, F={fano_factor}')
        k, P = samplefano.k2_prob_dist(mean, fano_factor)
        rv = samplefano.k2_rv(mean, fano_factor, size)
        chisq, ndof, p_value = samplefano.discrete_chisquare_test(k, P, rv)
        print(f'Chisquare = {chisq} / {ndof} dof, P-value = {p_value}')
        self.assertTrue(p_value > 1.e-6)

    def test_k3_stats(self, fano_factor=samplefano.CRITICAL_FANO):
        """Test the normalization and mean of the three-state probability distribution.
        """
        print(f'Testing k3 for F={fano_factor}')
        for mu in np.linspace(1. - fano_factor, 2. - fano_factor, 20):
            k, P = samplefano.k3_prob_dist(mu, fano_factor)
            norm, mean, variance = samplefano.discrete_prob_dist_stat(k, P)
            print(f'mu={mu:.4f}: {k} -> {P}')
            self.assertAlmostEqual(norm, 1.)
            self.assertAlmostEqual(mean, mu)
            self.assertAlmostEqual(variance, mu * fano_factor)

    def test_k3_random_variate(self, mean=1.5, fano_factor=samplefano.CRITICAL_FANO, size=1000000):
        """Test the k2 random variate.
        """
        print(f'Testing k3 random variate for mean={mean}, F={fano_factor}')
        k, P = samplefano.k3_prob_dist(mean, fano_factor)
        rv = samplefano.k3_rv(mean, fano_factor, size)
        chisq, ndof, p_value = samplefano.discrete_chisquare_test(k, P, rv)
        print(f'Chisquare = {chisq} / {ndof} dof, P-value = {p_value}')
        self.assertTrue(p_value > 1.e-6)

    def test_gen_poisson_stats(self, fano_factor=samplefano.CRITICAL_FANO):
        """
        """
        print(f'Testing generalized Poissono for F={fano_factor}')
        mu = np.linspace(2. - fano_factor, 10., 1000)
        norm = []
        mean = []
        variance = []
        for _mu in mu:
            k, P = samplefano.gen_poisson_prob_dist(_mu, fano_factor, normalize=True)
            n, m, v = samplefano.discrete_prob_dist_stat(k, P)
            norm.append(n)
            mean.append(m / _mu)
            variance.append(v / _mu / fano_factor)
        plt.figure('Generalized Poisson statistics')
        plt.plot(mu, norm, label='Normalization')
        plt.plot(mu, mean, label='Mean')
        plt.plot(mu, variance, label='Variance')
        plt.xlabel('Mean')
        plt.grid(which='both')
        plt.axis([None, None, 0.95, 1.05])
        plt.legend()




if __name__ == '__main__':
    unittest.main(exit=not sys.flags.interactive)
